grabspecs
=========

Fetches all (or selected) repositories from http://pkgs.fedoraproject.org/cgit/

Dependencies
------------

* GitPython

  ::

    sudo yum install GitPython -y

Usage
-----

$ ./fetch_pkgdb_list.sh

$ python grabspecs.py

References
----------

* https://fedoraproject.org/wiki/Using_the_package_database
