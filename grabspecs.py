#!/usr/bin/env python

import os
import multiprocessing
import threading
import sys
import git
from git import Repo

lock = threading.RLock()
baseurl = "http://pkgs.fedoraproject.org/git/"


def fetch(pkg):
    destination = os.path.join("repos", pkg)
    url = baseurl + pkg + ".git"
    if os.path.exists(destination):
        # return "[!] already have %s" % destination
        try:
            remote = Repo(destination).remote()
            remote.pull()
            return "[^] updating %s" % destination
        except:
            return "[!] failed updating %s" % destination

    try:
        Repo.clone_from(url, destination)
    except git.exc.GitCommandError:
        return "[!] failed cloning %s" % destination

    return "[+] cloned %s" % destination


def output_callback(data):
    with lock:
        print data
        sys.stdout.flush()

p = multiprocessing.Pool(16)

# read list of packages
pkgs = []
with open("pkgdb_list", "rb") as f:
    for line in f.readlines()[4:]:
        try:
            pkgname = line.split("|")[1]
            pkgs.append(pkgname)
        except IndexError:
            pass

for pkgname in pkgs:
    p.apply_async(fetch, (pkgname,), callback=output_callback)

p.close()
p.join()
